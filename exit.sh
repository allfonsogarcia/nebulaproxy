#!/bin/bash

# Start exiting
echo 'Exiting and shutdown docker containers'

# Enter into submodules and run docker compose
cd app1-reverse-proxy
docker compose down
cd ..

cd app2-reverse-proxy
docker compose down
cd ..

cd app-reverse-proxy
docker compose down
cd ..

