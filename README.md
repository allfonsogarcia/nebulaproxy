# NebulaProxy


## Getting started

Use git clone to get the repo files.
```
git clone git@gitlab.com:allfonsogarcia/nebulaproxy.git
```

It needs Docker 26.0.0 or later
Enter into repo folder and run the init.sh script
```
./init.sh
```

For closing and shutdown all the containers run the exit.sh script
```
./exit.sh
```

## Tree app

.
├── app1-reverse-proxy
│   ├── app-c1
│   │   ├── Dockerfile
│   │   ├── go.mod
│   │   ├── go.sum
│   │   ├── index.html
│   │   ├── main.go
│   │   └── settings.yaml
│   ├── compose.yaml
│   ├── lb1
│   │   ├── Dockerfile
│   │   └── nginx.conf
│   └── README.md
├── app2-reverse-proxy
│   ├── app-c2
│   │   ├── Dockerfile
│   │   ├── go.mod
│   │   ├── main.go
│   │   └── static
│   │       ├── index.html
│   │       └── styles.css
│   ├── compose.yaml
│   ├── lb2
│   │   ├── Dockerfile
│   │   └── nginx.conf
│   └── README.md
├── app-reverse-proxy
│   ├── compose.yaml
│   ├── README.md
│   └── reverse-proxy
│       ├── Dockerfile
│       ├── error404.html
│       └── nginx.conf
├── init.sh
└── README.md

## Used techologies 

All apps load balancers and the reverse proxy have the correct version and technologies installed inside.
The containers are running into an Nginx version with Alpine and web servers using go module http for servers.

- [ ] [Go] 
- [ ] [Nginx] 
- [ ] [Docker] 
- [ ] [Html] 
- [ ] [Css] 
- [ ] [JavaScript]

## Name
A name that conjures up the image of a proxy connecting different applications like stars in a nebula.

