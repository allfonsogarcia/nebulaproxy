#!/bin/bash

# Clone Submodules
git submodule update --init --recursive

# Enter into submodules and run docker compose
cd app1-reverse-proxy
docker compose up --build -d
cd ..

cd app2-reverse-proxy
docker compose up --build -d
cd ..

cd app-reverse-proxy
docker compose up --build -d
cd ..

